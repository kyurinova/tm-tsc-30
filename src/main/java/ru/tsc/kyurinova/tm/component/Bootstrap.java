package ru.tsc.kyurinova.tm.component;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.reflections.Reflections;
import ru.tsc.kyurinova.tm.api.repository.ICommandRepository;
import ru.tsc.kyurinova.tm.api.repository.IProjectRepository;
import ru.tsc.kyurinova.tm.api.repository.ITaskRepository;
import ru.tsc.kyurinova.tm.api.repository.IUserRepository;
import ru.tsc.kyurinova.tm.api.service.*;
import ru.tsc.kyurinova.tm.command.AbstractCommand;
import ru.tsc.kyurinova.tm.constant.TerminalConst;
import ru.tsc.kyurinova.tm.enumerated.Role;
import ru.tsc.kyurinova.tm.exception.system.UnknownCommandException;
import ru.tsc.kyurinova.tm.repository.CommandRepository;
import ru.tsc.kyurinova.tm.repository.ProjectRepository;
import ru.tsc.kyurinova.tm.repository.TaskRepository;
import ru.tsc.kyurinova.tm.repository.UserRepository;
import ru.tsc.kyurinova.tm.service.*;
import ru.tsc.kyurinova.tm.util.SystemUtil;

import java.io.File;
import java.lang.reflect.Modifier;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Comparator;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;

public final class Bootstrap implements IServiceLocator {

    @NotNull
    private final ICommandRepository commandRepository = new CommandRepository();

    @NotNull
    private final ICommandService commandService = new CommandService(commandRepository);

    @NotNull
    private final ITaskRepository taskRepository = new TaskRepository();

    @NotNull
    private final ITaskService taskService = new TaskService(taskRepository);

    @NotNull
    private final IProjectRepository projectRepository = new ProjectRepository();

    @NotNull
    private final IProjectService projectService = new ProjectService(projectRepository);

    @NotNull
    private final IProjectTaskService projectTaskService = new ProjectTaskService(taskRepository, projectRepository);

    @NotNull
    private final IUserRepository userRepository = new UserRepository();

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final IUserService userService = new UserService(userRepository, propertyService);

    @NotNull
    private final IAuthService authService = new AuthService(userService, propertyService);

    @NotNull
    private final ILogService logService = new LogService();

    @NotNull
    private final Backup backup = new Backup(this);

    public void start(@Nullable final String[] args) {
        System.out.println("** WELCOME TO TASK MANAGER **");
        initRegistry();
        initUsers();
        runArgs(args);
        initPID();
        backup.init();
        logService.debug("Test environment.");
        @NotNull final Scanner scanner = new Scanner(System.in);
        @NotNull String command = "";
        while (!TerminalConst.EXIT.equals(command)) {
            try {
                System.out.println("ENTER COMMAND:");
                command = scanner.nextLine();
                logService.command(command);
                runCommand(command);
                logService.info("Completed");
            } catch (final Exception e) {
                logService.error(e);
            }
        }
    }

    private void initRegistry() {
        @NotNull final Reflections reflections = new Reflections("ru.tsc.kyurinova.tm.command");
        @NotNull final List<Class<? extends AbstractCommand>> classes = reflections
                .getSubTypesOf(ru.tsc.kyurinova.tm.command.AbstractCommand.class)
                .stream()
                .sorted(Comparator.comparing(Class::getName))
                .collect(Collectors.toList());
        for (@NotNull final Class<? extends AbstractCommand> clazz : classes) {
            if (Modifier.isAbstract(clazz.getModifiers())) continue;
            try {
                registry(clazz.newInstance());
            } catch (@NotNull final Exception e) {
                logService.error(e);
            }
        }
    }

    private void initUsers() {
        userService.create("test", "test", "test@test.ru");
        userService.create("admin", "admin", Role.ADMIN);
    }

    private boolean runArgs(@Nullable final String[] args) {
        if (args == null || args.length == 0) return false;
        @Nullable final AbstractCommand command = commandService.getCommandByArg(args[0]);
        if (command == null) throw new UnknownCommandException();
        @Nullable final Role[] roles = command.roles();
        authService.checkRoles(roles);
        command.execute();
        return true;
    }

    @SneakyThrows
    private void initPID() {
        @NotNull final String filename = "task-manager.pid";
        @NotNull final String pid = Long.toString(SystemUtil.getPID());
        Files.write(Paths.get(filename), pid.getBytes());
        @NotNull final File file = new File(filename);
        file.deleteOnExit();
    }

    public void runCommand(@Nullable final String command) {
        if (command == null || command.isEmpty()) return;
        @Nullable final AbstractCommand abstractCommand = commandService.getCommandByName(command);
        if (abstractCommand == null) throw new UnknownCommandException();
        @Nullable final Role[] roles = abstractCommand.roles();
        authService.checkRoles(roles);
        abstractCommand.execute();
    }

    private void registry(@Nullable AbstractCommand command) {
        if (command == null) return;
        command.setServiceLocator(this);
        commandService.add(command);
    }

    @NotNull
    @Override
    public ITaskService getTaskService() {
        return taskService;
    }

    @NotNull
    @Override
    public IProjectService getProjectService() {
        return projectService;
    }

    @NotNull
    @Override
    public IProjectTaskService getProjectTaskService() {
        return projectTaskService;
    }

    @NotNull
    @Override
    public ICommandService getCommandService() {
        return commandService;
    }

    @NotNull
    @Override
    public IAuthService getAuthService() {
        return authService;
    }

    @NotNull
    @Override
    public IUserService getUserService() {
        return userService;
    }

    @NotNull
    @Override
    public IPropertyService getPropertyService() {
        return propertyService;
    }
}
