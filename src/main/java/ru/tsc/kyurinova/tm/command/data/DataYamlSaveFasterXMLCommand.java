package ru.tsc.kyurinova.tm.command.data;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kyurinova.tm.command.AbstractDataCommand;
import ru.tsc.kyurinova.tm.dto.Domain;

import java.io.FileOutputStream;

public class DataYamlSaveFasterXMLCommand extends AbstractDataCommand {

    @NotNull
    @Override
    public String name() {
        return "data-yaml-save";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Save data to yaml";
    }

    @Override
    @SneakyThrows
    public void execute() {
        System.out.println("[DATA YAML SAVE]");
        @NotNull final Domain domain = getDomain();
        @NotNull final YAMLFactory yamlfactory = new YAMLFactory();
        @NotNull final ObjectMapper objectMapper = new ObjectMapper(yamlfactory);
        @NotNull String yaml = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(domain);
        @NotNull final FileOutputStream fileOutputStream = new FileOutputStream(FILE_FASTERXML_YAML);
        fileOutputStream.write(yaml.getBytes());
        fileOutputStream.flush();
        fileOutputStream.close();
    }

}
