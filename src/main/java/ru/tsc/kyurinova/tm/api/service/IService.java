package ru.tsc.kyurinova.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kyurinova.tm.api.repository.IRepository;
import ru.tsc.kyurinova.tm.model.AbstractEntity;

import java.util.Collection;
import java.util.Comparator;
import java.util.List;

public interface IService<E extends AbstractEntity> extends IRepository<E> {

    void add(E entity);

    void addAll(@NotNull List<E> list);

    void remove(E entity);

    @NotNull
    List<E> findAll();

    @NotNull
    List<E> findAll(@NotNull Comparator<E> comparator);

    void clear();

    @Nullable
    E findById(@Nullable String id);

    @NotNull
    E findByIndex(@Nullable Integer index);

    @Nullable
    E removeById(@Nullable String id);

    @Nullable
    E removeByIndex(@Nullable Integer index);

    boolean existsById(@Nullable String id);

    boolean existsByIndex(@Nullable Integer index);

}
