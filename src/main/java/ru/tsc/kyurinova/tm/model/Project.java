package ru.tsc.kyurinova.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kyurinova.tm.api.entity.IWBS;
import ru.tsc.kyurinova.tm.enumerated.Status;

import java.util.Date;
import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
public class Project extends AbstractOwnerEntity implements IWBS {

    @NotNull
    public Project(@NotNull String name, @NotNull String description) {
        this.name = name;
        this.description = description;
    }

    @NotNull
    public Project(@NotNull String name, @NotNull String description, @Nullable Date startDate) {
        this.name = name;
        this.description = description;
        this.startDate = startDate;
    }

    @NotNull
    public Project(@NotNull String userId, @NotNull String name, @NotNull String description) {
        this.userId = userId;
        this.name = name;
        this.description = description;
    }

    @NotNull
    public Project(@NotNull String userId, @NotNull String name, @NotNull String description, @Nullable Date startDate) {
        this.userId = userId;
        this.name = name;
        this.description = description;
        this.startDate = startDate;
    }

    @NotNull
    private String name = "";

    @NotNull
    private String description = "";

    @NotNull
    private String userId = "";

    @NotNull
    private Status status = Status.NOT_STARTED;

    @Nullable
    private Date startDate;

    @Nullable
    private Date finishDate;

    @Nullable
    private Date created = new Date();

}
